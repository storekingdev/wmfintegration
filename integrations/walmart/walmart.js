/**
 * @author Aman Kareem <aman.kareem@storeking.in>
 * @since oct 2018;
 */

"use strict;"
var http = require("http");
var https = require("https");
var request = require("request");
var async = require("async");
var _ = require("lodash");
var events = require('events');
var moment = require("moment");
const OAuth = require('oauth-1.0a');
const crypto = require('crypto');
const qs = require('querystring');
var chalk = require('chalk');
var chalker = new chalk.constructor({ enabled: true, level: 1 });
const walmartConfig = require("./walmart.config.json");

/* 
    - Implement all functions from the abstraction layer;
    - Capillary / walmart swagger ui - http://www8.martjack.com/developerapi/swagger/ui/index#
*/

/** 
 * - Not Implemented
*/
function getAllCategories() {
    return new Promise((resolve, reject) => {

    });
}

/**
 * - This API gives only Parent categories;
 */
function getAllParentCategories(env) {
    return new Promise((resolve, reject) => {

        var config = _getConfig(env);

        var url = `${config.basePath}/category/${config.MerchantId}/NA`;

        _fire(config, url, 'GET', null).then(result => {
            resolve(result);
        }).catch(e => {
            reject(e);
        });
    });

}

/**
 * - Not Implemented;
 */
function getCategoryById() {
    return new Promise((resolve, reject) => {

    });
}

/**
 * - Not implemented
 */
function getParentCategory() {
    return new Promise((resolve, reject) => {

    });
}

/**
 * - This API for a given Parent Id, gets All its linked children;
 * - If isLeaf = false , then there are linked childrens;
 * - If isLeaf = true , then its the last children in the hierarchy;
 * - Field : Lineage - Indicates the hierarchy of the categories;
 */
function getCategoryChildren(env, parentId) {
    return new Promise((resolve, reject) => {

        var config = _getConfig(env);

        var url = `${config.basePath}/category/${config.MerchantId}/${parentId}`;

        _fire(config, url, 'GET', null).then(result => {
            resolve(result);
        }).catch(e => {
            reject(e);
        });
    });
}

/**
 * 
 */
function getProductsByCategory(env, categoryId) {
    return new Promise((resolve, reject) => {

        var config = _getConfig(env);

        var url = `${config.basePath}/Product/Category/${config.MerchantId}/${categoryId}`;

        _fire(config, url, 'GET', null).then(result => {
            resolve(result);
        }).catch(e => {
            reject(e);
        });
    });
}

/**
 * Not implemented
 */
function getAllProducts() {
    return new Promise((resolve, reject) => {

    });
}

/**
 * - All Products by location and pagination and with timestamp i,e delta;
 * - timestamp format = YYYYMMDDhhmmssSSS;
 * - moment(timestamp).format('YYYYMMDDhhmmssSSS')); 
 * - to - Indicates No. of products per page - Limit on out put
 * - from - page No.
 * - WrteTime= 20181210180000000: Timestamp in YYYYMMDDhhmmssSSS format.
 * - storeid=248 ; Location Id of walmart fc's
 */
function allProductsByLocation(env, fc, limit, page, timestamp) {
    return new Promise((resolve, reject) => {

        const config = _getConfig(env);

        const locationId = fc.partner.locationId;

        const url = `${config.frontApi}/Products/AllProducts/${config.MerchantId}`;

        limit = (limit !== undefined && limit !== null) ? limit : 50;

        page = page ? page : 1;

        timestamp = timestamp ? timestamp : -1; // -1 gets all products without considering any timestamp or delta for a given limit and page only;

        if (timestamp !== -1) {

            if (typeof timestamp === 'string') timestamp = new Date(timestamp);

            timestamp = moment(timestamp).format('YYYYMMDDhhmmssSSS');
        }

        console.log(chalker.green.bold(`All Products with timestamp : LocationId : ${locationId} / Limit Per Page : ${limit} / Page No. : ${page} / Timestamp : ${timestamp}`));

        const options = {
            query: {
                languagecode: `en`,
                to: limit,
                writetime: timestamp,
                from: page,
                storeid: locationId
            }
        }

        _fire(config, url, 'GET', options).then(data => {
            resolve(data);
        }).catch(e => reject(e));

    });
}

/**
 *
 */
function getProductById(env, productId) {
    return new Promise((resolve, reject) => {

        var config = _getConfig(env);

        var url = `${config.basePath}/Product/Information/${config.MerchantId}/${productId}`;

        _fire(config, url, 'GET', null).then(result => {
            resolve(result);
        }).catch(e => {
            reject(e);
        });

    });
}

/**
 * 
 */
function productImage(env, productId, width, height) {
    return new Promise((resolve, reject) => {

        var config = _getConfig(env);

        var url = `${config.basePath}/Product/Images/${config.MerchantId}/${productId}/${width}/${height}`;

        _fire(config, url, 'GET', null).then(result => {
            resolve(result);
        }).catch(e => {
            reject(e);
        });
    });
}

/**
 * 
 */
function productByLocation(env, fc, sku) {
    return new Promise((resolve, reject) => {

        const config = _getConfig(env);

        const locationId = fc.partner.locationId;

        const url = `${config.basePath}/Product/${config.MerchantId}/LocationStockPrice`;

        const headers = {
            CKey: config.consumerKey,
            Skey: config.secretkey,
        }

        const params = {
            query: { sKU: sku, locationId: locationId },
            headers: headers
        }

        _fire(config, url, 'POST', params).then(result => {

            if (result && result.LocationStockPrices && result.LocationStockPrices.length) {
                resolve(result.LocationStockPrices);
            } else {
                reject(new Error(`Could not get location-wise walmart inventory for SKU ${sku}`));
            }

        }).catch(e => {
            reject(e);
        });

    });
}

/**
 * Front end volumetric API
 */
function productInventory(env, fc, productId, isBuckets) {
    return new Promise((resolve, reject) => {

        const config = _getConfig(env);

        const locationId = fc.partner.locationId;

        const url = `${config.frontApi}/Products/${config.MerchantId}`; //?productid=336934&storeid=248&hsid=&outputformat=graph&isVolumetric=true

        const options = {
            query: {
                productid: productId,
                storeid: parseInt(locationId),
                hsid: null,
                outputformat: "graph",
                isVolumetric: true
            }
        }

        _fire(config, url, 'GET', options).then(result => {

            if (result && result.Products && result.Products.length) {
                if (isBuckets) {
                    let inventoryList = result.Products.map(p => {
                        let volumetricSlabs = p.VolumetricPricing;
                        volumetricSlabs = volumetricSlabs && volumetricSlabs.length ? volumetricSlabs : [];
                        volumetricSlabs = volumetricSlabs.map(v => {
                            return {
                                quantity: v.Quantity,
                                transferPrice: v.WebPrice
                            }
                        });
                        return {
                            LocationId: p.StoreID,
                            ProductId: p.ID,
                            Sku: p.Sku,
                            MRP: p.MRP,
                            WebPrice: p.WebPrice,
                            Inventory: p.Inventory,
                            volumetric: p.VolumetricPricing,
                            volumetricSlabs: volumetricSlabs
                        }
                    });
                    resolve(inventoryList);
                } else {
                    result.Products.map(p => {
                        let volumetricSlabs = p.VolumetricPricing;
                        volumetricSlabs = volumetricSlabs && volumetricSlabs.length ? volumetricSlabs : [];
                        volumetricSlabs = volumetricSlabs.map(v => {
                            return {
                                quantity: v.Quantity,
                                transferPrice: v.WebPrice
                            }
                        });
                        p.volumetricSlabs = volumetricSlabs;
                    });
                    resolve(result);
                }
            } else if (result && result.Products && !isBuckets) {
                resolve(result);
            } else {
                reject(new Error(`Could not get location-wise walmart inventory for ProductId ${productId}`));
            }

        }).catch(e => {
            reject(e);
        });

    });
}

/**
 * 
 */
function volumetricPricing(env, fc, productId) {
    return new Promise((resolve, reject) => {

        const config = _getConfig(env);

        const url = `${config.frontApi}/VolumetricPricing/${config.MerchantId}`;

        const options = {
            query: {
                productid: productId,
                storeId: fc.partner.locationId
            }
        }

        _fire(config, url, 'GET', options).then(result => {
            resolve(result);
        }).catch(e => {
            reject(e);
        });

    });
}


function inventoryBuckets(env, fc, productList, options) {
    return new Promise((resolve, reject) => {
        /*
            PAYLOAD:
           {
               "productId": product.id,
               "mapping" : {},
               "quantity" : product.quantity * deal.quantity,
               "dealId": deal._id,
               "whId": fc.whId,
               "locationId": 248
           } 
        */

        var config = _getConfig(env);

        var inventoryBuckets = {} //{"WMF3" : [ {"productId" : "PR123" , mapping: {} , snapshost: [] } ]}

        var walmartProductList = [];

        var grouped = _.groupBy(productList, "whId");

        Object.keys(grouped).map(whId => {

            inventoryBuckets[whId] = []; //Array of unique products only for that fc;

            let productList = grouped[whId];

            productList.map(p => {
                var record = _.find(inventoryBuckets[whId], { "productId": p.productId });
                if (!record) {//If not there for this whId/fc/location then only push, else skip;
                    var newRecord = {
                        "productId": p.productId,
                        "mapping": p.mapping,
                        "whId": p.whId,
                        "locationId": p.locationId,
                        "totalQty": 0,
                        "snapShots": [],
                        "buckets": {},
                        "bucketKeys": []
                    };
                    inventoryBuckets[whId].push(newRecord);
                }
            });

        });

        //Iterate Inventory Buckets location-wise , and for each locations , iterate products and get inventory of the product;
        Promise.all(Object.keys(inventoryBuckets).map(whId => {
            return new Promise((resolve, reject) => {

                let errorList = [];

                let productGroup = inventoryBuckets[whId]; //This list is unique for a given fc/whId/locationId;

                var queue = async.queue((p, queueCB) => {

                    let fcData = { 'partner': { 'locationId': p.locationId } };

                    const sku = p.mapping.sku;

                    productByLocation(env, fcData, sku).then(parnterInventories => {
                        /**
                         * TODO::
                         *  - Push to snapshots;
                         *  - Form buckets
                         *  - resolve
                         */
                        if (parnterInventories && parnterInventories.length) {

                            parnterInventories = _.sortBy(parnterInventories, ['WebPrice']);
                            //Form snapshots;
                            parnterInventories.map(inventory => {
                                var snapShot = {
                                    productId: p.productId,
                                    whId: p.whId,
                                    mappedProductId: p.mapping.productId,
                                    mrp: inventory.MRP,
                                    transferPrice: inventory.WebPrice,
                                    stock: parseInt(inventory.Inventory),
                                    onHold: 0,
                                    key: `${inventory.MRP}`,
                                    snapShotIds: []
                                };
                                p.snapShots.push(snapShot);
                            });

                            p.totalQty = _.sumBy(p.snapShots, 'stock');
                            //Form buckets;
                            p.buckets = _.groupBy(p.snapShots, 'key');
                            //Form bucketKeys;
                            p.bucketKeys = Object.keys(p.buckets);
                            //Buckets summarisation;

                            Object.keys(p.buckets).map(key => {
                                var bucketGroup = p.buckets[key];
                                var count = _.sumBy(bucketGroup, 'stock');
                                p.buckets[key] = {
                                    productId: bucketGroup[0].productId,
                                    whId: bucketGroup[0].whId,
                                    mappedProductId: bucketGroup[0].mappedProductId,
                                    mrp: bucketGroup[0].mrp,
                                    transferPrice: bucketGroup[0].transferPrice,
                                    stock: count,
                                    onHold: 0,
                                    snapShotIds: []
                                }
                            });

                            queueCB(null);

                        } else {
                            queueCB(new Error(`Could not get location-wise inventory for Product ${p.productId}`));
                        }

                    }).catch(e => queueCB(e));

                });

                queue.push(productGroup, (e, result) => {
                    if (e) {
                        productGroup = [];
                        errorList.push(e);
                    }
                });

                queue.drain = () => {
                    if (!errorList || !errorList.length) {
                        resolve();
                    } else {
                        reject(errorList);
                    }
                };

            });
        })).then(() => {

            resolve({ buckets: inventoryBuckets });

        }).catch(e => reject(e));

    });
}

function inventoryBuckets_V2(env, fc, productList, options) {
    return new Promise((resolve, reject) => {
        /*
            PAYLOAD:
           {
               "productId": product.id,
               "mapping" : {},
               "quantity" : product.quantity * deal.quantity,
               "dealId": deal._id,
               "whId": fc.whId,
               "locationId": 248
           } 
        */

        var config = _getConfig(env);

        var inventoryBuckets = {} //{"WMF3" : [ {"productId" : "PR123" , mapping: {} , snapshost: [] } ]}

        var walmartProductList = [];

        var grouped = _.groupBy(productList, "whId");

        Object.keys(grouped).map(whId => {

            inventoryBuckets[whId] = []; //Array of unique products only for that fc;

            let productList = grouped[whId];

            productList.map(p => {
                var record = _.find(inventoryBuckets[whId], { "productId": p.productId });
                if (!record) {//If not there for this whId/fc/location then only push, else skip;
                    var newRecord = {
                        "productId": p.productId,
                        "mapping": p.mapping,
                        "whId": p.whId,
                        "locationId": p.locationId,
                        "totalQty": 0,
                        "snapShots": [],
                        "buckets": {},
                        "bucketKeys": []
                    };
                    inventoryBuckets[whId].push(newRecord);
                }
            });

        });

        //Iterate Inventory Buckets location-wise , and for each locations , iterate products and get inventory of the product;
        Promise.all(Object.keys(inventoryBuckets).map(whId => {
            return new Promise((resolve, reject) => {

                let errorList = [];

                let productGroup = inventoryBuckets[whId]; //This list is unique for a given fc/whId/locationId;

                var queue = async.queue((p, queueCB) => {

                    let fcData = { 'partner': { 'locationId': p.locationId } };

                    const sku = p.mapping.sku;

                    const productId = p.mapping.productId;

                    productInventory(env, fcData, productId, true).then(parnterInventories => {
                        /**
                         * TODO::
                         *  - Push to snapshots;
                         *  - Form buckets
                         *  - resolve
                         */
                        if (parnterInventories && parnterInventories.length) {

                            parnterInventories = _.sortBy(parnterInventories, ['WebPrice']);
                            //Form snapshots;
                            parnterInventories.map(inventory => {
                                var snapShot = {
                                    productId: p.productId,
                                    whId: p.whId,
                                    mappedProductId: p.mapping.productId,
                                    mrp: inventory.MRP,
                                    transferPrice: inventory.WebPrice,
                                    stock: typeof inventory.Inventory === 'string' ? parseInt(inventory.Inventory) : inventory.Inventory,
                                    onHold: 0,
                                    key: `${inventory.MRP}`,
                                    snapShotIds: [],
                                    volumetric: inventory.volumetric
                                };
                                p.snapShots.push(snapShot);
                            });

                            p.totalQty = _.sumBy(p.snapShots, 'stock');
                            //Form buckets;
                            p.buckets = _.groupBy(p.snapShots, 'key');
                            //Form bucketKeys;
                            p.bucketKeys = Object.keys(p.buckets);
                            //Buckets summarisation;

                            Object.keys(p.buckets).map(key => {
                                var bucketGroup = p.buckets[key];
                                var count = _.sumBy(bucketGroup, 'stock');
                                p.buckets[key] = {
                                    productId: bucketGroup[0].productId,
                                    whId: bucketGroup[0].whId,
                                    mappedProductId: bucketGroup[0].mappedProductId,
                                    mrp: bucketGroup[0].mrp,
                                    transferPrice: bucketGroup[0].transferPrice,
                                    stock: count,
                                    onHold: 0,
                                    snapShotIds: [],
                                    volumetric: bucketGroup[0].volumetric
                                }
                            });

                            queueCB(null);

                        } else {
                            queueCB(new Error(`Could not get location-wise inventory for Product ${p.productId}`));
                        }

                    }).catch(e => queueCB(e));

                });

                queue.push(productGroup, (e, result) => {
                    if (e) {
                        productGroup = [];
                        errorList.push(e);
                    }
                });

                queue.drain = () => {
                    if (!errorList || !errorList.length) {
                        resolve();
                    } else {
                        reject(errorList);
                    }
                };

            });
        })).then(() => {

            resolve({ buckets: inventoryBuckets });

        }).catch(e => reject(e));

    });
}

function intoBuckets(productList, inventoryList) {

    var inventoryBuckets = {} //{"WMF3" : [ {"productId" : "PR123" , mapping: {} , snapshost: [] } ]}

    var walmartProductList = [];

    var grouped = _.groupBy(productList, "whId");

    Object.keys(grouped).map(whId => {

        inventoryBuckets[whId] = []; //Array of unique products only for that fc;

        let productList = grouped[whId];

        productList.map(p => {
            var record = _.find(inventoryBuckets[whId], { "productId": p.productId });
            if (!record) {//If not there for this whId/fc/location then only push, else skip;
                var newRecord = {
                    "productId": p.productId,
                    "mapping": p.mapping,
                    "whId": p.whId,
                    "locationId": p.locationId,
                    "totalQty": 0,
                    "snapShots": [],
                    "buckets": {},
                    "bucketKeys": []
                };
                inventoryBuckets[whId].push(newRecord);
            }
        });

    });

    Object.keys(inventoryBuckets).map(whId => {

        let productGroup = inventoryBuckets[whId]; //This list is unique for a given fc/whId/locationId;

        productGroup.map(p => {

            let fcData = { 'partner': { 'locationId': p.locationId } };
            const sku = p.mapping.sku;

            var parnterInventories = extractInventories(inventoryList, p.mapping.productId, p.mapping.sku, p.locationId);

            if (parnterInventories && parnterInventories.length) {

                parnterInventories = _.sortBy(parnterInventories, ['WebPrice']);
                //Form snapshots;
                parnterInventories.map(inventory => {
                    var snapShot = {
                        productId: p.productId,
                        whId: p.whId,
                        mappedProductId: p.mapping.productId,
                        mrp: inventory.MRP,
                        transferPrice: inventory.WebPrice,
                        stock: typeof inventory.Inventory === 'string' ? parseInt(inventory.Inventory) : inventory.Inventory,
                        onHold: 0,
                        key: `${inventory.MRP}`,
                        snapShotIds: []
                    };
                    p.snapShots.push(snapShot);
                });

                p.totalQty = _.sumBy(p.snapShots, 'stock');
                //Form buckets;
                p.buckets = _.groupBy(p.snapShots, 'key');
                //Form bucketKeys;
                p.bucketKeys = Object.keys(p.buckets);
                //Buckets summarisation;

                Object.keys(p.buckets).map(key => {
                    var bucketGroup = p.buckets[key];
                    var count = _.sumBy(bucketGroup, 'stock');
                    p.buckets[key] = {
                        productId: bucketGroup[0].productId,
                        whId: bucketGroup[0].whId,
                        mappedProductId: bucketGroup[0].mappedProductId,
                        mrp: bucketGroup[0].mrp,
                        transferPrice: bucketGroup[0].transferPrice,
                        stock: count,
                        onHold: 0,
                        snapShotIds: []
                    }
                });

            }

        });

    });

    function extractInventories(inventoryList, productId, sku, locationId) {
        let productIds = [productId.toString(), parseInt(productId)];
        let skus = [sku.toString(), parseInt(sku)];
        let locationIds = [locationId.toString(), parseInt(locationId)];

        return inventoryList.filter(inv => productIds.indexOf(inv.ProductId) > -1 && skus.indexOf(inv.SKU) > -1 && locationIds.indexOf(inv.LocationId) > -1 ? true : false);
    }

    return inventoryBuckets;

}


/**
 * 
 */
function startSession(env, fc) {
    return new Promise((resolve, reject) => {

        const config = _getConfig(env);

        const url = `${config.basePath}/Customer/${config.MerchantId}/StartCustomerSession`;

        const locationId = fc.partner.locationId;

        const options = {
            query: {
                username: fc.partner.userId,
                password: fc.partner.password,
                operatorId: config.operatorId,
                locationId: locationId
            },
            headers: {
                CKey: config.consumerKey,
                SKey: config.secretkey
            }
        }

        _fire(config, url, 'POST', options).then(result => {
            if (result && result.messageCode === '1004' && result.Token) {
                resolve(result.Token);
            } else {
                reject(new Error(`Login Error , Could not start customer session ....`));
            }
        }).catch(e => reject(e));

    });
}


/**
 * 
 */
function accessToken(env) {
    return new Promise((resolve, reject) => {

        var config = _getConfig(env);

        var url = `${config.basePath}/Customer/GetAccessToken/${config.MerchantId}`;

        _fire(config, url, 'POST', null).then(result => {

            if (result && result.ErrorCode === 0 && result.Token) {
                resolve(result);
            } else {
                reject(new Error(`Could not get access token...`));
            }

        }).catch(e => {
            reject(e);
        });

    });
}

/**
 * 
 */
function login(env, fc) {
    return new Promise((resolve, reject) => {

        var config = _getConfig(env);

        const url = `${config.basePath}/Customer/${config.MerchantId}/Login/true/${fc.partner.locationId}`;

        const options = {
            query: {
                username: fc.partner.userId,
                password: fc.partner.password
            },
            headers: {
                CKey: config.consumerKey,
                SKey: config.secretkey
            }
        }

        _fire(config, url, 'POST', options).then(result => {
            if (result && result.messageCode === '1004' && result.Token) {
                resolve(result.Token);
            } else {
                reject(new Error(`Login Error , Could not access token ....`));
            }
        }).catch(e => reject(e));

    });
}

/**
 * 
 */
function logout(env, fc, token) {
    return new Promise((resolve, reject) => {

        var config = _getConfig(env);

        const url = `${config.basePath}/Customer/${config.MerchantId}/Logout`;

        const options = {
            headers: {
                CKey: config.consumerKey,
                SKey: config.secretkey,
                Ver: 3,
                AccessToken: token.AccessToken
            }
        }

        _fire(config, url, 'POST', options).then(result => {
            if (result && result.messageCode === '1004') {
                resolve(result);
            } else {
                reject(new Error(`Logout Error , Could not logout....`));
            }
        }).catch(e => reject(e));

    });
}

/**
 * 
 * NOTE: Token parameter is not mandatory; If passed it will not make an API call , if passed it will use the token parameter;
 */
function addToCart(env, fc, order, token) {
    return new Promise((resolve, reject) => {

        var config = _getConfig(env);

        var error = [];

        var cartResult = [];

        var productList = [];

        order.subOrders.map(sO => {
            productList = productList.concat(sO.products);
        });

        //Get token;
        var loginPromise = null;

        if (!token) {
            loginPromise = login(env, fc).catch(e => reject(e));
        } else {
            loginPromise = Promise.resolve(token);
        }

        loginPromise.then(loginToken => {

            const accessToken = loginToken.AccessToken;

            const headers = {
                CKey: config.consumerKey,
                Skey: config.secretkey,
                Ver: 3,
                AccessToken: accessToken
            }

            var queue = async.queue(function (product, queueCB) {

                var url = `${config.basePath}/Carts/Add/${config.MerchantId}/${product.mapping.productId}/0/${product.quantity}/delveryMode=H`;

                const options = { headers: headers };

                _fire(config, url, 'GET', options).then(result => {
                    if (result.messageCode === "1004") {
                        queueCB(null, result);
                    } else {
                        queueCB(result);
                    }
                }).catch(e => {
                    queueCB(e);
                });

            });

            queue.push(productList, (err, result) => {
                if (err) {
                    error.push(err);
                    productList = [];// make it empty so that the queue wont proceed;
                    reject(err);
                    return;
                }
                if (result) {
                    cartResult.push(result);
                }
            });

            queue.drain = function () {
                resolve({ result: cartResult, error: error });
            }

        }).catch(e => reject(e));

    });
}

/**
 * - Adds based on location
 * - uses access token to determine location
 */
function addToCartBySession(env, fc, order, token) {
    return new Promise((resolve, reject) => {

        var config = _getConfig(env);

        var error = [];

        var cartResult = [];

        var productList = [];

        order.subOrders.map(sO => {
            productList = productList.concat(sO.products);
        });

        //Get token;
        var loginPromise = null;

        if (!token) {
            loginPromise = startSession(env, fc).catch(e => reject(e));
        } else {
            loginPromise = Promise.resolve(token);
        }

        loginPromise.then(loginToken => {

            const accessToken = loginToken.AccessToken;

            const headers = {
                CKey: config.consumerKey,
                Skey: config.secretkey,
                Ver: 3,
                AccessToken: accessToken
            }

            var queue = async.queue(function (product, queueCB) {

                const url = `${config.basePath}/Carts/Add/${config.MerchantId}/${product.mapping.productId}/0/${product.quantity}/${accessToken}?delveryMode=H`;

                const options = { headers: headers };

                _fire(config, url, 'GET', options).then(result => {
                    if (result.messageCode === "1004") {
                        queueCB(null, result);
                    } else {
                        queueCB(result);
                    }
                }).catch(e => {
                    queueCB(e);
                });

            });

            queue.push(productList, (err, result) => {
                if (err) {
                    error.push(err);
                    productList = [];// make it empty so that the queue wont proceed;
                    reject(err);
                    return;
                }
                if (result) {
                    cartResult.push(result);
                }
            });

            queue.drain = function () {
                resolve({ result: cartResult, error: error });
            }

        }).catch(e => reject(e));

    });
}

/**
 * - Addes based on location
 */
function addToCartByLocation(env, fc, order, token) {
    return new Promise((resolve, reject) => {

        const config = _getConfig(env);

        const locationId = fc.partner.locationId;

        var error = [];

        var cartResult = [];

        var productList = [];

        order.subOrders.map(sO => {
            productList = productList.concat(sO.products);
        });

        //Get token;
        var loginPromise = null;

        if (!token) {
            loginPromise = startSession(env, fc).catch(e => reject(e));
        } else {
            loginPromise = Promise.resolve(token);
        }

        loginPromise.then(loginToken => {

            const accessToken = loginToken.AccessToken;

            const headers = {
                CKey: config.consumerKey,
                Skey: config.secretkey,
                Ver: 3,
                AccessToken: accessToken
            }

            var queue = async.queue(function (product, queueCB) {

                const url = `${config.basePath}/Carts/LocationCart/${config.MerchantId}/${product.mapping.productId}/0/${product.quantity}/${locationId}/rs?delveryMode=H`;

                const options = { headers: headers };

                _fire(config, url, 'GET', options).then(result => {
                    if (result.messageCode === "1004") {
                        queueCB(null, result);
                    } else {
                        queueCB(result);
                    }
                }).catch(e => {
                    queueCB(e);
                });

            });

            queue.push(productList, (err, result) => {
                if (err) {
                    error.push(err);
                    productList = [];// make it empty so that the queue wont proceed;
                    reject(err);
                    return;
                }
                if (result) {
                    cartResult.push(result);
                }
            });

            queue.drain = function () {
                resolve({ result: cartResult, error: error });
            }


        }).catch(e => reject(e));

    });
}

/**
 * 
 */
function addCartItems(env, fc, order, token) {
    return new Promise((resolve, reject) => {

        var config = _getConfig(env);

        var loginPromise = null;

        if (!token) {
            loginPromise = login(env, fc).catch(e => reject(e));
        } else {
            loginPromise = Promise.resolve(token);
        }

        loginPromise.then(loginToken => {

            const accessToken = loginToken.AccessToken;

            const headers = {
                CKey: config.consumerKey,
                Skey: config.secretkey,
                Ver: 3,
                AccessToken: accessToken,
                'Content-Type': "application/x-www-form-urlencoded"
            }

            const url = `${config.basePath}/Carts/AddCartItems/${config.MerchantId}`;

            const locationId = fc.partner.locationId;

            let cartItems = [];

            order.subOrders.map(sO => sO.products.map(p => {
                let item = {
                    Status: 'A',
                    VariantProductID: '0',
                    LocationId: locationId,
                    CartReferenceKey: '00000000-0000-0000-0000-000000000000',
                    ProductID: p.mapping.productId,
                    Quantity: p.quantity
                };
                cartItems.push(item);
            }));

            let payload = `MerchantId=${config.MerchantId}&InputFormat=application/json&InputData=${encodeURIComponent(JSON.stringify({ cart: { item: cartItems } }))}`;

            let options = {
                headers: headers,
                body: payload
            }

            _fire(config, url, 'POST', options).then(result => {
                resolve(result);
            }).catch(e => reject(e));

        }).catch(e => reject(e));

    });
}

/**
 * 
 * NOTE: Token parameter is not mandatory; If passed it will not make an API call , if passed it will use the token parameter;
 */
function viewCart(env, fc, token) {
    return new Promise((resolve, reject) => {

        var config = _getConfig(env);

        console.log(chalker.blue.bold(` [ ${env.msg} ] Fetching cart for Merchant-Id ${config.MerchantId} .....`));

        //Get token;
        var loginPromise = null;

        if (!token) {
            loginPromise = login(env, fc).catch(e => reject(e));
        } else {
            loginPromise = Promise.resolve(token);
        }

        loginPromise.then(loginToken => {

            const accessToken = loginToken.AccessToken;

            const headers = {
                CKey: config.consumerKey,
                Skey: config.secretkey,
                Ver: 1,
                AccessToken: accessToken
            };

            const url = `${config.basePath}/Carts/Cart/${config.MerchantId}`;

            const options = { headers: headers };

            _fire(config, url, 'GET', options).then(result => {
                if (result) {
                    resolve(result);
                } else {
                    reject(new Error(`Cart Not Found`));
                }
            }).catch(e => reject(e));

        }).catch(e => reject(e));

    });
}

/**
 * 
 */
function removeFromCart() {
    return new Promise((resolve, reject) => {

    });
}

/** 
 * 
*/
function replaceItemsInCart() {
    return new Promise((resolve, reject) => {

    });
}

/**
 * 
 */
function clearCart(env, fc) {
    return new Promise((resolve, reject) => {

        const config = _getConfig(env);

        const url = `${config.basePath}/Carts/Clear/${config.MerchantId}`;

        const options = {
            headers: {
                CKey: config.consumerKey,
                Skey: config.secretkey,
            }
        }

        _fire(config, url, 'GET', options).then(result => {
            if (result && result.messageCode === '1004') {
                resolve(result);
            } else {
                reject(new Error(`Cart Error , Could not clear cart....`));
            }
        }).catch(e => reject(e));

    });
}

/**
 * 
 */
function removeAllFromCart(env, fc, token) {
    return new Promise((resolve, reject) => {

        let tokenPromise = null;

        if (!token) {
            tokenPromise = login(env, fc).catch(e => reject(e));
        } else {
            tokenPromise = Promise.resolve(token);
        }

        tokenPromise.then(loginToken => {

            const config = _getConfig(env);

            const accessToken = loginToken.AccessToken;

            const url = `${config.basePath}/Carts/RemoveAll/${config.MerchantId}`;

            const options = {
                headers: {
                    CKey: config.consumerKey,
                    Skey: config.secretkey,
                    Ver: 1,
                    AccessToken: accessToken
                }
            }

            _fire(config, url, 'GET', options).then(result => {
                if (result) {
                    resolve(result);
                } else {
                    reject(new Error(`Could not remove all items from cart ...`));
                }
            }).catch(e => reject(e));

        }).catch(e => reject(e));

    });
}

/**
 * Place Order
 * TODO:::
 *  - Get Access Token;
 *  - Get shipping modes;
 *  - Change shipping mode in cart;
 *  - Checkout / Add to cart;
 *  - Query order information , after checkout and resolve;
 * 
 * NOTE: Token parameter is not mandatory; If passed it will not make an API call , if passed it will use the token parameter;
 */
function checkOut(env, fc, token) {
    return new Promise((resolve, reject) => {

        const config = _getConfig(env);

        var params = { env: env, fc: fc, config: config, token: token };

        async.waterfall([
            _getToken(params),
            _getShipmentModes,
            _changeShippingMode,
            _placeOrder,
            _getOrder
        ], function (err, result) {
            if (err)
                reject(err);
            else
                resolve(result);
        });

        function _getToken(params) {
            return function (callback) {

                if (!params.token) {
                    login(params.env, params.fc).then(token => {
                        params.token = token;
                    }).catch(e => callback(e));
                } else {
                    callback(null, params);
                }

            }
        }

        function _getShipmentModes(params, callback) {

            shippmentModes(params.env, params.fc, params.token).then(mode => {
                params.shipmentMode = mode.Carts.ShippingOptions[0].ShippingMode;
                callback(null, params);
            }).catch(e => callback(e));

        }

        function _changeShippingMode(params, callback) {

            changeShipmentMode(params.env, params.fc, params.fc.partner.shipmentMode, params.token).then(data => {
                params.modeChanged = true;
                callback(null, params);
            }).catch(e => callback(e));

        }

        function _placeOrder(params, callback) {

            const config = params.config;

            const fc = params.fc;

            const url = `${config.basePath}/Order/PlaceOrder/${config.MerchantId}`;

            const query = {
                paymentOption: fc.partner.paymentOption,
                paymentType: fc.partner.paymentType,
                gatewayId: fc.partner.gatewayId,
                channelType: null
            }

            const headers = {
                CKey: config.consumerKey,
                Skey: config.secretkey,
                Ver: 1,
                AccessToken: params.token.AccessToken
            };

            const options = { query: query, headers: headers };

            _fire(config, url, 'POST', options).then(result => {
                params.checkout = result;
                params.orderId = result.OrderCreationResponse.OrderID;
                callback(null, params);
            }).catch(e => callback(e));

        }

        function _getOrder(params, callback) {
            var orderId = params.orderId;
            getOrder(params.env, orderId).then(order => {
                params.order = order;
                callback(null, params);
            }).catch(e => callback(e));
        }

    });
}


/**
 * TODO:::
 * #1. Login - Use Customer Login API;
 * #2. Remove Cart - Use Remove all from cart API;
 * #3. Add Cart Items - Use API to add cart items - Post API;
 * #4. Update address;
 * #5. Get shipping modes;
 * #6. Change shipping mode;
 * #7. Fetch updated cart;
 * #8. Place order - Use place order V2 - params in body;
 * #9. Authorise order;
 * #10. Get order;
 * #11. Logout
 */
function addAndCheckout(env, fc, data) {
    return new Promise((resolve, reject) => {

        let params = { env: env, fc: fc, data: data, locationId: fc.partner.locationId, status: 'Failed' };

        async.waterfall([
            _login(params), //#1.
            _removeAllFromCart, //#2.
            _addCartItems, //#3.
            _updateAddress,//#4.
            _getShipmentModes,//#5.
            _changeShipmentMode,//#6.
            _fetchCart,//#7.
            _placeOrder,//#8
            _authorizeOrder,//#9.
            _getOrder,//#10.
            _logout//#11.
        ], function (err, result) {
            if (err)
                reject(new Error(JSON.stringify(params)));
            else {
                delete params.fc;
                delete params.env;
                delete params.data;
                resolve(result);
            }
        });
        // #1.
        function _login(params) {
            return function (callback) {
                login(params.env, params.fc)
                    .then(token => {
                        params.token = token;
                        params.isLoggedIn = true;
                        params.MerchantId = token.MerchantId;
                        callback(null, params);
                    })
                    .catch(e => {
                        params.error = {
                            log: `Error occured while logging in for location Id ${params.locationId}`,
                            error: e
                        }
                        callback(e);
                    });
            };
        }
        //#2.
        function _removeAllFromCart(params, callback) {
            removeAllFromCart(params.env, params.fc, params.token).then(result => {
                params.clearCart = result;
                params.isCartCleared = true;
                callback(null, params);
            }).catch(e => {
                params.error = {
                    log: `Error occured while removing/clearing cart items for location Id ${params.locationId}`,
                    error: e
                }
                callback(e);
            });
        }
        //#3.
        function _addCartItems(params, callback) {
            addCartItems(params.env, params.fc, params.data, params.token)
                .then(cart => {
                    params.cart = cart;
                    params.isAddedToCart = true;
                    callback(null, params);
                }).catch(e => {
                    params.error = {
                        log: `Error occured while adding items to cart for location Id ${params.locationId}`,
                        error: e
                    }
                    callback(e);
                });
        }
        //#4.
        function _updateAddress(params, callback) {
            updateShipmentAddress(params.env, params.fc, params.token).then(result => {
                params.updateCartAddress = result;
                params.isAddressUpdated = true;
                callback(null, params);
            }).catch(e => {
                params.error = {
                    log: `Error occured while updating cart shipping address for location Id ${params.locationId}`,
                    error: e
                }
                callback(e);
            });
        }
        //#5.
        function _getShipmentModes(params, callback) {
            /* shippmentModes(params.env, params.fc, params.token).then(mode => {
                params.shipmentMode = mode.Carts.ShippingOptions[0].ShippingMode;
                callback(null, params);
            }).catch(e => {
                params.error = {
                    log: `Error occured while fetching shipment modes for location Id ${params.locationId}`,
                    error: e
                }
                callback(e);
            }); */

            // Set Shipment mode as InStorePickUp - inorder to avoid shipping costs;
            const config = _getConfig(params.env);
            params.shipmentMode = params.fc.partner.shipmentMode;
            callback(null, params);
        }
        //#6.
        function _changeShipmentMode(params, callback) {
            changeShipmentMode(params.env, params.fc, params.fc.partner.shipmentMode, params.token).then(data => {
                params.modeChanged = true;
                callback(null, params);
            }).catch(e => {
                params.error = {
                    log: `Error occured while updating shipment modes for location Id ${params.locationId}`,
                    error: e
                }
                callback(e);
            });
        }
        //#7.
        function _fetchCart(params, callback) {
            viewCart(params.env, params.fc, params.token).then(updatedCart => {
                params.cart = updatedCart;
                params.hasCartView = true;
                callback(null, params);
            }).catch(e => {
                params.error = {
                    log: `Error occured while fetching cart view for location Id ${params.locationId}`,
                    error: e
                }
                callback(e);
            });
        }
        //#8.
        function _placeOrder(params, callback) {
            placeOrder(params.env, params.fc, params.token).then(order => {
                params.checkout = order;
                params.status = "Pending";
                params.orderId = order.OrderCreationResponse.OrderID;
                params.orderId = params.orderId.toString();
                params.isOrderPlaced = true;
                callback(null, params);
            }).catch(e => {
                params.status = "Failed";
                params.error = {
                    log: `Error occured while Placing order for location Id ${params.locationId}`,
                    error: e
                }
                callback(e);
            });
        }
        //#9.
        function _authorizeOrder(params, callback) {

            const fc = params.fc;

            if (fc && !fc.partner.autoAuthorize) {
                params.status = "Success";
                params.isAuthorised = false;
                params.autoAuthorize = fc.partner.autoAuthorize;
                callback(null, params);
                return;
            }

            authorizeOrder(params.env, params.fc, params.orderId).then(data => {
                params.authorize = data;
                params.status = "Success";
                params.isAuthorised = true;
                params.autoAuthorize = fc.partner.autoAuthorize;
                callback(null, params);
            }).catch(e => {
                params.isAuthorised = false;
                params.error = {
                    log: `Error occured while Authorising order ${params.orderId} for location Id ${params.locationId}`,
                    error: e
                }
                callback(e);
                // callback(null, params);
            });
        }
        //#10.
        function _getOrder(params, callback) {
            var orderId = params.orderId;
            getOrder(params.env, orderId).then(order => {
                params.order = order;
                params.hasOrderView = true;
                params.partnerStatus = order.Orders[0].Status;
                if (params.partnerStatus === 'A') {
                    params.status = "Success";
                } else {
                    params.status = "Pending";
                }
                callback(null, params);
            }).catch(e => {
                params.error = {
                    log: `Error occured while fetching order view for ${params.orderId} for location Id ${params.locationId}`,
                    error: e
                }
                callback(e);
            });
        }
        //#11.
        function _logout(params, callback) {
            logout(params.env, params.fc, params.token).then(result => {
                params.logout = result;
                params.isLoggedOut = true;
                callback(null, params);
            }).catch(e => {
                params.error = {
                    log: `Error occured while logging out for location Id ${params.locationId}`,
                    error: e
                }
                callback(e);
            });
        }

    });
}

/**
 * 
 */
function placeOrder(env, fc, token) {
    return new Promise((resolve, reject) => {

        let loginPromise = null;

        if (!token) {
            loginPromise = startSession(env, fc).catch(e => reject(e));
        } else {
            loginPromise = Promise.resolve(token);
        }

        loginPromise.then(token => {

            const config = _getConfig(env);

            const url = `${config.basePath}/Order/PlaceOrder/${config.MerchantId}`;

            const query = {
                paymentOption: fc.partner.paymentOption,
                paymentType: fc.partner.paymentType,
                gatewayId: fc.partner.gatewayId,
                channelType: null
            }

            const headers = {
                CKey: config.consumerKey,
                Skey: config.secretkey,
                Ver: 1,
                AccessToken: token.AccessToken
            };

            const options = { query: query, headers: headers };

            _fire(config, url, 'POST', options).then(result => {
                resolve(result);
            }).catch(e => reject(e));


        }).catch(e => reject(e));

    });
}

/**
 * 
 */
function authorizeOrder(env, fc, orderId) {
    return new Promise((resolve, reject) => {

        const config = _getConfig(env);

        const url = `${config.basePath}/Order/Authorize`;

        const options = {
            /*  query: {
                 merchantId: config.MerchantId,
                 orderId: orderId,
                 paymentType: 'COD',
                 pGResponse: 'yes',
                 bankInstrumentNumber: '2689666',
                 bankName: 'Kotak'
             }, */
            headers: {
                CKey: config.consumerKey,
                Skey: config.secretkey,
            },
            body: `merchantId=${config.MerchantId}&orderId=${orderId}&paymentType=${fc.partner.paymentOption}&bankInstrumentNumber=${fc.partner.bankInstrumentNumber}&bankName=${fc.partner.bankName}&pGResponse=yes`
        }

        _fire(config, url, 'POST', options).then(result => resolve(result)).catch(e => reject(e));

    });
}

/**
 * 
 */
function getOrder(env, orderId) {
    return new Promise((resolve, reject) => {

        var config = _getConfig(env);

        const url = `${config.basePath}/Order/${config.MerchantId}/${orderId}`;

        _fire(config, url, 'GET', null).then(order => {
            resolve(order);
        }).catch(e => reject(e));

    });
}

/**
 * 
 */
function cancelOrder() {
    return new Promise((resolve, reject) => {

    });
}

/**
 * 
 */
function trackOrder() {
    return new Promise((resolve, reject) => {

    });
}

/**
 * 
 */
function shippmentModes(env, fc, token) {
    return new Promise((resolve, reject) => {

        const config = _getConfig(env);

        let loginPromise = null;

        if (!token) {
            loginPromise = login(env, fc).catch(e => reject(e));
        } else {
            loginPromise = Promise.resolve(token);
        }

        loginPromise.then(token => {

            const url = `${config.basePath}/Carts/ShippingMode/${config.MerchantId}`;

            const headers = {
                CKey: config.consumerKey,
                Skey: config.secretkey,
                Ver: 1,
                AccessToken: token.AccessToken
            };

            const options = { headers: headers };

            _fire(config, url, 'GET', options).then(modes => {
                resolve(modes);
            }).catch(e => reject(e));

        }).catch(e => reject(e));

    });
}

/**
 * 
 */
function changeShipmentMode(env, fc, mode, token) {
    return new Promise((resolve, reject) => {

        const config = _getConfig(env);

        let loginPromise = null;

        if (!token) {
            loginPromise = login(env, fc).catch(e => reject(e));
        } else {
            loginPromise = Promise.resolve(token);
        }

        loginPromise.then(token => {

            const url = `${config.basePath}/Carts/ShippingMode/${config.MerchantId}/${mode}/Change/true`;

            const headers = {
                CKey: config.consumerKey,
                Skey: config.secretkey,
                Ver: 1,
                AccessToken: token.AccessToken
            };

            const options = { headers: headers };

            _fire(config, url, 'GET', options).then(result => {
                resolve(result);
            }).catch(e => reject(e));


        }).catch(e => reject(e));

    });
}

/**
 * 
 */
function updateShipmentAddress(env, fc, token) {
    return new Promise((resolve, reject) => {
        //FailedItems if this array , then reject error;
        const config = _getConfig(env);

        let loginPromise = null;

        if (!token) {
            loginPromise = login(env, fc).catch(e => reject(e));
        } else {
            loginPromise = Promise.resolve(token);
        }

        loginPromise.then(loginToken => {

            const accessToken = loginToken.AccessToken;

            const url = `${config.basePath}/Carts/UpdateAddress/${config.MerchantId}`;

            const headers = {
                CKey: config.consumerKey,
                Skey: config.secretkey,
                Ver: 3,
                AccessToken: accessToken,
                'Content-Type': "application/x-www-form-urlencoded"
            }

            /* let address = {

                "GiftMessage": "Javed Iqbal-25336",

                "BillFirstName": "SUROB KIRANA AND GENERAL STORES",

                "BillLastName": "KHAN",

                "BillAddress1": "GULISTA NAGAR NEAR BY BIG WELL AMRAVATI",

                "BillAddress2": "ABC",

                "BillCountry": "IN",

                "BillCountryCode": "IN",

                "BillState": "UP",

                "BillCity": "1",

                "BillOtherCity": "",

                "BillTelephoneCode": "",

                "BillTelephone": "",

                "BillMobileCode": "",

                "BillMobile": "91-9890150792",

                "BillZip": "282007",

                "BillEmail": "pharukkhan@Gmail.com",

                "ShipFirstName": "SUROB KIRANA AND GENERAL STORES",

                "ShipLastName": "Test Lasname",

                "ShipAddress1": "GULISTA NAGAR NEAR BY BIG WEL AMRAVATI",

                "ShipAddress2": "",

                "ShipCountry": "IN",

                "ShipCountryCode": "IN",

                "ShipState": "UP",

                "ShipCity": "6",

                "ShipOtherCity": "",

                "ShipTelephoneCode": "",

                "ShipTelephone": "91-9890150792",

                "ShipMobileCode": "",

                "ShipMobile": "91-9890150792",

                "ShipZip": "282007",

                "IsShippingAddressDifferent": "False"

            }; */

            let address = {

                "GiftMessage": "",

                "BillFirstName": fc && fc.companyName ? fc.companyName : "",

                "BillLastName": "",

                "BillAddress1": fc && fc.address ? `${fc.address.door_no ? fc.address.door_no : ""} , ${fc.address.street ? fc.address.street : ""} ` : "",//"GULISTA NAGAR NEAR BY BIG WELL AMRAVATI",

                "BillAddress2": fc && fc.address ? `${fc.address.landmark ? fc.address.landmark : ""} , ${fc.address.full_address ? fc.address.full_address : ""} ` : "",

                "BillCountry": "IN",

                "BillCountryCode": "IN",

                "BillState": fc.partner.billStateCode ? fc.partner.billStateCode : "", // check

                "BillCity": fc.partner.billCityCode ? fc.partner.billCityCode : "", // check

                "BillOtherCity": "",

                "BillTelephoneCode": "",

                "BillTelephone": "",

                "BillMobileCode": "",

                "BillMobile": fc && fc.contacts && fc.contacts.length ? `91-${fc.contacts[0].mobile}` : "",//"91-9890150792",

                "BillZip": fc.partner.billZip ? fc.partner.billZip : "", // check

                "BillEmail": fc && fc.contacts && fc.contacts.length ? `${fc.contacts[0].email}` : "",

                "ShipFirstName": fc && fc.companyName ? fc.companyName : "",

                "ShipLastName": "",

                "ShipAddress1": fc && fc.address ? `${fc.address.door_no} , ${fc.address.street} ` : "",

                "ShipAddress2": fc && fc.address ? `${fc.address.landmark} , ${fc.address.full_address} ` : "",

                "ShipCountry": "IN",

                "ShipCountryCode": "IN",

                "ShipState": fc.partner.billStateCode ? fc.partner.billStateCode : "", //check

                "ShipCity": fc.partner.billCityCode ? fc.partner.billCityCode : "", //check

                "ShipOtherCity": "",

                "ShipTelephoneCode": "",

                "ShipTelephone": fc && fc.contacts && fc.contacts.length ? `91-${fc.contacts[0].mobile}` : "",

                "ShipMobileCode": "",

                "ShipMobile": fc && fc.contacts && fc.contacts.length ? `91-${fc.contacts[0].mobile}` : "",

                "ShipZip": fc.partner.billZip ? fc.partner.billZip : "", //check

                "IsShippingAddressDifferent": "False"

            };

            let payload = `MerchantId=${config.MerchantId}&InputFormat=application/json&InputData=${encodeURIComponent(JSON.stringify({ Cart: address }))}`;

            let options = {
                headers: headers,
                body: payload
            }

            _fire(config, url, 'POST', options).then(result => {
                resolve(result);
            }).catch(e => reject(e));

        }).catch(e => reject(e));

    });
}

/**
 * 
 */
function getLocationInformation(env, fc) {
    return new Promise((resolve, reject) => {

        const config = _getConfig(env);

        const locationId = fc.partner.locationId;

        const url = `${config.basePath}/Location/Information/${config.MerchantId}/${locationId}`;

        const options = {
            headers: {
                CKey: config.consumerKey,
                SKey: config.secretkey
            }
        }

        _fire(config, url, 'GET', options).then(location => resolve(location)).catch(e => reject(e));

    });
}

/**
 * 
 */
function getStates(env, fc) {
    return new Promise((resolve, reject) => {

        const config = _getConfig(env);

        const url = `${config.basePath}/Store/States/${config.MerchantId}/IN`;

        const options = {
            headers: {
                CKey: config.consumerKey,
                SKey: config.secretkey
            }
        }

        _fire(config, url, 'GET', options).then(states => resolve(states)).catch(e => reject(e));

    });
}

/**
 * 
 */
function getCities(env, fc, stateCode) {
    return new Promise((resolve, reject) => {

        const config = _getConfig(env);

        const url = `${config.basePath}/Store/Cities/${config.MerchantId}/${stateCode}`;

        const options = {
            headers: {
                CKey: config.consumerKey,
                SKey: config.secretkey
            }
        }

        _fire(config, url, 'GET', options).then(cities => resolve(cities)).catch(e => reject(e));

    });
}

/**
 * 
 */
function getConfiguredLocations(env, fc) {
    return new Promise((resolve, reject) => {

        const config = _getConfig(env);

        const url = `${config.basePath}/Location/${config.MerchantId}/GetCities`;

        const options = {
            headers: {
                CKey: config.consumerKey,
                SKey: config.secretkey
            }
        }

        _fire(config, url, 'GET', options).then(location => resolve(location)).catch(e => reject(e));

    });
}

/**
 * Search Products by productIds against a given locationId;
 * Takes Select array for projection;
 */
function productSearch(env, fc, params) {
    return new Promise((resolve, reject) => {

        console.log(chalker.green(`Running Product search ...... ${params.productIdList && params.productIdList.length ? params.productIdList.length : 0}`));

        const config = _getConfig(env);

        const locationId = fc.partner.locationId;

        const isVolumetric = params.isVolumetric !== undefined ? params.isVolumetric : false;

        const productIdList = params.productIdList && params.productIdList.length ? _.uniq(params.productIdList) : [];

        const select = params.select && params.select.length ? params.select : [];

        let result = [];

        const url = `${config.frontApi}/Products/${config.MerchantId}`; //?productid=336934&storeid=248&hsid=&outputformat=graph&isVolumetric=true

        const queue = async.queue(function (productId, queueCB) {

            const options = {
                query: {
                    productid: productId,
                    storeid: locationId,
                    hsid: null,
                    outputformat: "graph",
                    isVolumetric: isVolumetric
                }
            }

            _fire(config, url, 'GET', options).then(result => {
                if (result && result.Products && result.Products.length) {

                    let productList = result.Products.map(p => {

                        let obj = {};

                        Object.keys(p).map(k => {
                            if (select && select.length && select.indexOf(k) > -1) {
                                obj[k] = p[k];
                            } else if (select && !select.length) {
                                obj[k] = p[k];
                            }
                        });

                        return obj;
                    });

                    queueCB(null, productList[0]);

                } else {
                    queueCB(null);
                }
            }).catch(e => reject(e));
        }, 10);

        queue.push(productIdList, function (err, data) {
            if (err) {
                queue.tasks = [];
                queue.kill();
                reject(err);
            } else if (data) {
                result.push(data);
            }
        });

        queue.drain = function () {
            resolve(result);
        }

    });
}


/**
 * 
 */
function bulkProductSearch(env, fc, params) {
    return new Promise((resolve, reject) => {

    });
}

/**
 * 
 */
function _getConfig(env) {
    if (env.isProd) {
        return walmartConfig.production;
    } else {
        return walmartConfig.sandbox;
    }
}

function getOAuth(config, url, method, params) {

    const oauth = OAuth({
        consumer: {
            key: config.consumerKey,
            secret: config.secretkey
        },
        signature_method: 'HMAC-SHA1',
        nounce_length: 32,
        version: '1.0',
        hash_function(base_string, key) {
            return crypto.createHmac('sha1', key).update(base_string).digest('base64');
        }
    });

    const _url = params.query && !_.isEmpty(params.query) ? `${url}?${qs.stringify(params.query)}` : url;

    const _method = method

    const options = {
        url: _url,
        method: _method,
    };

    return oauth.authorize(options);
}

function _fire(config, url, method, params) {
    return new Promise((resolve, reject) => {

        if (!url) {
            reject(new Error(`URL cannot be empty for making HTTP call...`));
            return
        }
        if (!method) {
            reject(new Error(`HTTP method cannot be empty...`));
            return
        }
        if (!config) {
            reject(new Error(`Config is required to make requests to the integration layer....`));
            return
        }

        try {

            params = params ? params : {};

            const oAuth = getOAuth(config, url, method, params);

            const _headers = params.headers && !_.isEmpty(params.headers) ? Object.assign({ "Accept": "application/json" }, params.headers) : { "Accept": "application/json" };

            const _query = params.query && !_.isEmpty(params.query) ? Object.assign(params.query, oAuth) : oAuth;

            const _body = params.body && !_.isEmpty(params.body) ? params.body : {};

            const options = {
                url: url,
                method: method,
                qs: _query,
                form: _body,
                headers: _headers,
            }

            request(options, function (error, response, body) {
                if (error) {
                    reject(error);
                } else if (body) {

                    var data = JSON.parse(body);

                    if (data.messageCode === '1004' || data.messageCode === '1005' || data.MessageCode === "200") {
                        resolve(data);
                    } else {
                        reject(data);
                    }

                } else {
                    reject(new Error("Integration layer , Error occured , could not get response.."));
                }
            });

        } catch (e) {
            reject(e);
        }

    });
}


module.exports = {
    login: login,
    logout: logout,
    getAllCategories: getAllCategories,
    getAllParentCategories: getAllParentCategories,
    getCategoryById: getCategoryById,
    getParentCategory: getParentCategory,
    getCategoryChildren: getCategoryChildren,
    getProductsByCategory: getProductsByCategory,
    getAllProducts: getAllProducts,
    allProductsByLocation: allProductsByLocation,
    getProductById: getProductById,
    productImage: productImage,
    productByLocation: productByLocation,
    productInventory: productInventory,
    volumetricPricing: volumetricPricing,
    inventoryBuckets: inventoryBuckets_V2,//inventoryBuckets,
    intoBuckets: intoBuckets,
    startSession: startSession,
    accessToken: accessToken,
    addToCart: addToCart,
    addToCartBySession: addToCartBySession,
    addToCartByLocation: addToCartByLocation,
    addCartItems: addCartItems,
    viewCart: viewCart,
    removeFromCart: removeFromCart,
    replaceItemsInCart: replaceItemsInCart,
    clearCart: clearCart,
    removeAllFromCart: removeAllFromCart,
    checkOut: checkOut,
    addAndCheckout: addAndCheckout,
    placeOrder: placeOrder,
    getOrder: getOrder,
    authorizeOrder: authorizeOrder,
    cancelOrder: cancelOrder,
    trackOrder: trackOrder,
    shippmentModes: shippmentModes,
    changeShipmentMode: changeShipmentMode,
    updateShipmentAddress: updateShipmentAddress,
    getLocationInformation: getLocationInformation,
    getConfiguredLocations: getConfiguredLocations,
    getStates: getStates,
    getCities: getCities,
    productSearch: productSearch
}