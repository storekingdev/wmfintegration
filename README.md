
# WMF's INTERGRATION LAYER

### INSTALLATION
``` sh
    npm install git+https://bitbucket.org/storekingdev/wmfintegration.git --save
```

``` sh
package.json: 
    {
         "wmfintegration": "git+https://bitbucket.org/storekingdev/wmfintegration.git"
    }
```

### SET UP
```sh
    var wmfIntergration = require("wmfintegration");
    wmfIntergration = new wmfIntergration();
    wmfIntergration.init(process.env, logger);
```